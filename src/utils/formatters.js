import { isNumber } from 'lodash';
import { DateTime } from 'luxon';

export function numberFormatter(value, noValue = '') {
  if (!isNumber(value)) return noValue;
  const number = value
    .toString()
    .replace(',', '.');
  return (number.includes('.')
    ? number.replace(/(?=(\d{3})+\.)/g, ' ')
    : number.replace(/(?=(\d{3})+$)/g, ' ')).trim();
}

export function dateFormatter(
  value,
  { format = 'dd.MM.yyyy', noValue = '' } = {},
) {
  if (!value) return noValue;
  const date = DateTime.isDateTime(value)
    ? value.toUTC()
    : DateTime.fromISO(value, { zone: 'utc' });
  return date.isValid ? date.toLocal().toFormat(format) : noValue;
}
