import Vue from 'vue';
import Vuex from 'vuex';
import api from '@/api';

Vue.use(Vuex);

export default new Vuex.Store({

  state: () => ({
    data: [],
    isLoading: false,
    isCached: false,
  }),

  mutations: {
    setState(state, value) {
      Object.entries(value).forEach(([key, data]) => {
        if (!Array.isArray(state[key]) && state[key] && typeof state[key] === 'object') {
          state[key] = {
            ...state[key],
            ...data,
          };
        } else {
          state[key] = data;
        }
      });
    },
  },

  actions: {
    async load({ commit }, params = { cache: true }) {
      commit('setState', { isLoading: true });

      try {
        const cache = params.cache && localStorage.getItem('payments');

        const { data } = cache
          ? { data: JSON.parse(cache) }
          : await api.getPayments();
        commit('setState', { isCached: !!cache });

        if (!Array.isArray(data)) return;
        commit('setState', { data });

        localStorage.setItem('payments', JSON.stringify(data));
      } catch (e) {
        // eslint-disable-next-line no-alert
        alert(e?.message);
      } finally {
        commit('setState', { isLoading: false });
      }
    },
  },
});
